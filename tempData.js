// Temporary Data until we connect with Firebase

export default tempData = [
    {
        name: "IOT",
        color: "#24A6D9",
        startTime:"14:00",
        endTime:"15:00",
        location:"CAMT 102",
        id:"953102",
        date:"Friday",
        
    },
    {
        name: "OOAD",
        color: "#8022D9",
        startTime:"14:00",
        endTime:"15:00",
        location:"CAMT 213",
        id:"953104",
        date:"Monday",
        
    },
    {
        name: "ADT",
        color: "#595BD9",
        startTime:"14:00",
        endTime:"15:00",
        location:"CAMT 307",
        id:"953107",
        date:"Friday",
        
    }
];
