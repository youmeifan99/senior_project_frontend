import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Modal } from 'react-native';
import colors from '../Colors';
import TodoModal from './TodoModal';

export default class TodoList extends React.Component {
    // const completedCount = list.todos.filter(todo => todo.completed).length;
    // const remainingCount = list.todos.length - completedCount;
    state = {
        showListVisible: false,
    };

    toggleListModal() {
        this.setState({ showListVisible: !this.state.showListVisible });
    }

    render() {
        const list = this.props.list;
        return (
            <View>
                <Modal
                    animationType="slide"
                    visible={this.state.showListVisible}
                    onRequestClose={() => this.toggleListModal()}
                >
                    <TodoModal
                        list={list}
                        closeModal={() => this.toggleListModal()}
                    />
                </Modal>

                <TouchableOpacity
                    style={[
                        styles.listContainer,
                        { backgroundColor: list.color },
                    ]}
                    onPress={() => this.toggleListModal()}
                >
                    <Text style={styles.listTitle} numberOfLines={1}>
                        {list.name}
                    </Text>

                    <View>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={styles.time}>{list.startTime}</Text>
                            <Text style={styles.subtitle}>Time</Text>
                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <Text style={styles.time}>{list.location}</Text>
                            <Text style={styles.subtitle}>Location</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    listContainer: {
        paddingVertical: 32,
        paddingHorizontal: 16,
        borderRadius: 6,
        marginHorizontal: 12,
        alignItems: 'center',
        width: 200,
    },
    listTitle: {
        fontSize: 24,
        fontWeight: '700',
        color: colors.white,
        marginBottom: 18,
    },
    time: {
        fontSize: 34,
        fontWeight: '200',
        color: colors.white,
    },
    subtitle: {
        fontSize: 12,
        fontWeight: '700',
        color: colors.white,
    },
});
