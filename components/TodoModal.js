import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    Alert,
    Modal,
} from 'react-native';
import { AntDesign, Ionicons } from '@expo/vector-icons';
import colors from '../Colors';
import { Divider, Icon } from 'react-native-elements';
import AddListModal from './AddListModal';
import StudentManage from './StudentManage';
import EditModal from './EditModal';
import axios from 'axios';

export default class TodoModal extends React.Component {
    state = {
        name: this.props.list.name,
        color: this.props.list.color,
        todos: this.props.list.todos,
        startTime: this.props.list.startTime,
        endTime: this.props.list.endTime,
        location: this.props.list.location,
        id: this.props.list._id,
        day: this.props.list.day,
        addTodoVisible: false,
        studentListVisible: false,
        updateListVisible: false,
    };

    toggleAddTodoModal() {
        this.setState({ addTodoVisible: !this.state.addTodoVisible });
    }
    toggleStudentListVisible() {
        this.setState({ studentListVisible: !this.state.studentListVisible });
    }
    toggleUpdateListVisible() {
        this.setState({ updateListVisible: !this.state.updateListVisible });
    }

    deleteCourse() {
        axios
            .delete(`http://localhost:5000/course/${this.state.id.$oid}`)
            .then((res) => {
                if (res) {
                    console.log('deleted', this.state.id.$oid);
                }
            });

        this.props.closeModal();
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <TouchableOpacity
                    style={{
                        position: 'absolute',
                        top: 64,
                        right: 32,
                        zIndex: 10,
                    }}
                    onPress={this.props.closeModal}
                >
                    <AntDesign name="close" size={24} color={colors.black} />
                </TouchableOpacity>

                <View
                    style={[
                        styles.section,
                        styles.header,
                        { borderBottomColor: this.state.color },
                    ]}
                >
                    <View>
                        <Text style={styles.title}>{this.state.name}</Text>
                    </View>
                </View>

                <View style={[styles.section, { flex: 3 }]}>
                    <Text style={styles.todo}>
                        Start Time: {this.state.startTime}
                    </Text>
                    <Text style={styles.todo}>
                        End Time: {this.state.endTime}
                    </Text>
                    <Text style={styles.todo}>Date: {this.state.day}</Text>
                    <Text style={styles.todo}>
                        Location: {this.state.location}
                    </Text>
                </View>
                <View style={styles.row}>
                    <Modal
                        animationType="slide"
                        visible={this.state.addTodoVisible}
                        onRequestClose={() => this.toggleAddTodoModal()}
                    >
                        <AddListModal
                            closeModal={() => this.toggleAddTodoModal()}
                        />
                    </Modal>
                    <Modal
                        animationType="slide"
                        visible={this.state.studentListVisible}
                        onRequestClose={() => this.toggleStudentListVisible()}
                    >
                        <StudentManage
                            closeModal={() => this.toggleStudentListVisible()}
                        />
                    </Modal>
                    <Modal
                        animationType="slide"
                        visible={this.state.updateListVisible}
                        onRequestClose={() => this.toggleUpdateListVisible()}
                    >
                        <EditModal
                            list={this.props.list}
                            closeModal={() => this.toggleUpdateListVisible()}
                        />
                    </Modal>
                    {/* use modal to make page can redirect */}
                    <Icon
                        reverse
                        name="ios-create"
                        type="ionicon"
                        onPress={() => this.toggleUpdateListVisible()}
                    />
                    <Icon
                        reverse
                        name="ios-contact"
                        type="ionicon"
                        onPress={() => this.toggleStudentListVisible()}
                    />
                    <Icon
                        reverse
                        name="ios-trash"
                        type="ionicon"
                        color="#CA300E"
                        onPress={() =>
                            Alert.alert(
                                'Delete?',
                                'Cannot be undone',
                                [
                                    { text: 'Cancel' },
                                    {
                                        text: 'OK',
                                        onPress: () => {
                                            this.deleteCourse();
                                        },
                                    },
                                ],
                                { cancelable: false }
                            )
                        }
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    section: {
        flex: 1,
        alignSelf: 'stretch',
    },
    header: {
        justifyContent: 'flex-end',
        marginLeft: 64,
        borderBottomWidth: 3,
    },
    title: {
        fontSize: 30,
        fontWeight: '800',
        color: colors.black,
    },
    taskCount: {
        marginTop: 4,
        marginBottom: 16,
        color: colors.gray,
        fontWeight: '600',
    },
    footer: {
        paddingHorizontal: 32,
        flexDirection: 'row',
        alignItems: 'center',
    },

    todo: {
        color: colors.blue,
        fontWeight: '700',
        fontSize: 20,
        paddingVertical: 16,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        marginTop: 16,
        marginBottom: 16,
        paddingLeft: 16,
        paddingRight: 16,
    },
});
