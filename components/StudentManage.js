import React, { Component } from 'react';
import {
    AppRegistry,
    FlatList,
    StyleSheet,
    Text,
    View,
    Image,
    Alert,
    SafeAreaView,
    TouchableOpacity,
} from 'react-native';
import { List, ListItem, Icon } from 'react-native-elements';
import Swipeout from 'react-native-swipeout';
import { AntDesign, Ionicons } from '@expo/vector-icons';

class FlatListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeRowKey: null,
        };
    }
    render() {
        const swipeSettings = {
            autoClose: true,
            onClose: (secId, rowId, direction) => {
                if (this.state.activeRowKey != null) {
                    this.setState({ activeRowKey: null });
                }
            },
            onOpen: (secId, rowId, direction) => {
                this.setState({ activeRowKey: this.props.item._id.$oid });
            },
            right: [
                {
                    onPress: () => {
                        const deletingRow = this.state.activeRowKey;
                        Alert.alert(
                            'Alert',
                            'Are you sure you want to delete ?',
                            [
                                {
                                    text: 'No',
                                    onPress: () =>
                                        console.log('Cancel Pressed'),
                                    style: 'cancel',
                                },
                                {
                                    text: 'Yes',
                                    onPress: () => {
                                        this.deleteData();
                                        //Refresh FlatList !
                                        this.props.parentFlatList.refreshFlatList(
                                            deletingRow
                                        );
                                    },
                                },
                            ],
                            { cancelable: true }
                        );
                    },
                    text: 'Delete',
                    type: 'delete',
                },
            ],
            // rowId: this.props.index,
            sectionId: 1,
        };

        return (
            <Swipeout {...swipeSettings}>
                <View
                    style={{
                        flex: 1,
                        backgroundColor: 'mediumseagreen',
                    }}
                >
                    <Text style={styles.flatListItem}>
                        {this.props.item.firstName} {this.props.item.lastName}
                    </Text>
                    {/* <Text style={styles.flatListItem}>{this.props.item._id.$oid}</Text> */}
                </View>
            </Swipeout>
        );
    }
}

export default class StudentManage extends React.Component {
    state = {
        data: [],
    };

    componentDidMount() {
        this.fetchData();
    }

    fetchData = async () => {
        const response = await fetch('http://localhost:5000/students');
        const json = await response.json();
        this.setState({ data: json });
    };

    //   deleteData() {
    //   return fetch('http://localhost:5000/students' + '/' + this.item._id.$oid, {
    //     method: 'DELETE'
    //   }).then(response =>
    //     response.json().then(json => {
    //       return json;
    //     })
    //   );
    // }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <TouchableOpacity
                    style={{
                        position: 'absolute',
                        top: 10,
                        right: 32,
                        zIndex: 10,
                    }}
                    onPress={this.props.closeModal}
                >
                    <AntDesign name="close" size={24} color={colors.black} />
                </TouchableOpacity>
                <View style={{ flex: 1, marginTop: 45 }}>
                    <FlatList
                        data={this.state.data}
                        renderItem={({ item }) => {
                            //console.log(`Item = ${JSON.stringify(item)}, index = ${index}`);
                            return <FlatListItem item={item}></FlatListItem>;
                        }}
                    ></FlatList>
                </View>
            </SafeAreaView>
        );
    }

    // return (
    //   <Swipeout right={swipeBtns}
    //     autoClose='true'
    //     backgroundColor= 'transparent'>
    //     <TouchableHighlight
    //       underlayColor='rgba(192,192,192,1,0.6)'
    //       onPress={this.viewNote.bind(this, rowData)} >
    //       <View>
    //         <View style={styles.rowContainer}>
    //           <Text style={styles.note}> {rowData.title} </Text>
    //         </View>
    //         <Separator />
    //       </View>
    //     </TouchableHighlight>
    //   </Swipeout>
    // )
}
//   render() {
//     return (
//       <View style={styles.container}>
//         <FlatList
//           data={this.state.data}
//            keyExtractor={(x, i) => i}
//           renderItem={({ item }) =>
//           <Text>
//               {`${item.firstName} ${item.lastName}`}

//             </Text>}

//         />
//         </View>
//     );
//   }
// }

const styles = StyleSheet.create({
    container: {
        marginTop: 45,
        flex: 1,
    },
    row: {
        padding: 15,
        marginBottom: 5,
        backgroundColor: 'skyblue',
    },
    flatListItem: {
        color: 'white',
        padding: 10,
        fontSize: 16,
        marginTop: 10,
        marginBottom: 10,
    },
});
