import {
    View,
    Text,
    StyleSheet,
    KeyboardAvoidingView,
    TouchableOpacity,
    TextInput,
    ScrollView,
    SafeAreaView,
} from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import colors from '../Colors';
import tempData from '../tempData';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import React from 'react';
import SectionedMultiSelect from 'react-native-multiple-select';
import axios from 'axios';

const items = [
    { id: '1', name: 'Monday' },
    { id: '2', name: 'Tuesday' },
    { id: '3', name: 'Wednesday' },
    { id: '4', name: 'Thursday' },
    { id: '5', name: 'Friday' },
    { id: '6', name: 'Saturday' },
    { id: '7', name: 'Sunday' },
];

export default class AddListModal extends React.Component {
    backgroundColors = [
        '#5CD859',
        '#24A6D9',
        '#595BD9',
        '#8022D9',
        '#D159D8',
        '#D85963',
        '#D88559',
    ];

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            color: '',
            location: '',
            courseId: '',
            day: [],
            color: this.backgroundColors[0],
            // isVisible1: false,
            // isVisible2: false,
            startTime: null,
            endTime: null,
        };
    }

    timechange1 = (event, time) => {
        console.log(time);
        this.setState({
            startTime: time,
        });
    };

    timechange2 = (event, time) => {
        this.setState({
            endTime: time,
        });
    };

    createTodo = (event) => {
        // const data = new FormData();
        //   data.append('name', this.state.name);
        //   data.append('startTime', this.state.startTime);
        //   data.append('endTime', this.state.endTime);
        //   data.append('location', this.state.location);
        //   data.append('day', this.state.day);
        //   data.append('color', this.state.color);
        //   data.append('courseId', this.state.courseId);

        const card = {
            name: this.state.name,
            startTime: moment(this.state.startTime).format('HH:mm'),
            endTime: moment(this.state.endTime).format('HH:mm'),
            location: this.state.location,
            day: this.state.day,
            color: this.state.color,
            courseId: this.state.courseId,
        };
        // console.log('===========================', card);
        axios.post('http://localhost:5000/courses', card).then((res) => {
            if (res) {
                console.log('added', card);
            }
        });
        this.props.closeModal();
    };

    renderColors() {
        return this.backgroundColors.map((color) => {
            return (
                <TouchableOpacity
                    key={color}
                    style={[styles.colorSelect, { backgroundColor: color }]}
                    onPress={() => this.setState({ color })}
                    name="color"
                    value={this.state.color}
                />
            );
        });
    }

    onSelectedItemsChange = (day) => {
        this.setState({ day });
    };

    handleStartTimePicker = (time) => {
        console.log('start time has been chosed', time);
        this.setState({
            startTime: time,
            isVisible1: false,
        });
    };

    handleEndTimePicker = (time) => {
        console.log('End time has been chosed', time);
        this.setState({
            endTime: time,
            isVisible2: false,
        });
    };

    hideStartTimePicker = () => {
        this.setState({
            isVisible1: false,
        });
    };

    hideEndTimePicker = () => {
        this.setState({
            isVisible2: false,
        });
    };

    showStartTimePicker = () => {
        this.setState({
            isVisible1: true,
        });
    };

    showEndTimePicker = () => {
        this.setState({
            isVisible2: true,
        });
    };

    render() {
        return (
            <ScrollView style={{ marginTop: 100 }}>
                <KeyboardAvoidingView
                    style={styles.container}
                    behavior="padding"
                >
                    <TouchableOpacity
                        style={{ position: 'absolute', top: 10, right: 20 }}
                        onPress={this.props.closeModal}
                    >
                        <AntDesign
                            name="close"
                            size={24}
                            color={colors.black}
                        />
                    </TouchableOpacity>

                    <View
                        style={{ alignSelf: 'stretch', marginHorizontal: 32 }}
                    >
                        <Text style={styles.title}>Create Course</Text>
                        <Text>Course name:</Text>
                        <TextInput
                            style={styles.input}
                            placeholder="Course Name?"
                            onChangeText={(text) =>
                                this.setState({ name: text })
                            }
                            name="name"
                            value={this.state.name}
                        />
                        <Text>Course ID:</Text>
                        <TextInput
                            style={styles.input}
                            keyboardType="number-pad"
                            textContentType="telephoneNumber"
                            maxLength={6}
                            placeholder="Course ID?"
                            onChangeText={(text) =>
                                this.setState({ courseId: text })
                            }
                            name="courseId"
                            value={this.state.courseId}
                        />

                        <View>
                            <Text>StartTime:</Text>
                            <TouchableOpacity
                                style={styles.button}
                                onPress={this.showStartTimePicker}
                            >
                                <Text style={styles.text}>
                                    Choose start time
                                </Text>
                            </TouchableOpacity>
                            <DateTimePicker
                                date={new Date()}
                                cancelTextIOS={'Exit'}
                                confirmTextIOS={'OK'}
                                isVisible={this.state.isVisible1}
                                onConfirm={this.handleStartTimePicker}
                                onCancel={this.hideStartTimePicker}
                                mode={'time'}
                                format={'HH:mm'}
                                is24Hour={true}
                                datePickerModeAndroid={'spinner'}
                                textColor={colors.black}
                                name="startTime"
                                value={this.state.startTime}
                                onChange={this.timechange1}
                            />

                            <Text
                                style={{
                                    color: 'black',
                                    fontSize: 20,
                                    margin: 10,
                                }}
                            >
                                {this.state.startTime !== null &&
                                    moment(this.state.startTime).format(
                                        'HH:mm'
                                    )}
                            </Text>
                        </View>

                        <View>
                            <Text>EndTime:</Text>
                            <TouchableOpacity
                                style={styles.button}
                                onPress={this.showEndTimePicker}
                            >
                                <Text style={styles.text}>Choose End time</Text>
                            </TouchableOpacity>
                            <DateTimePicker
                                date={new Date()}
                                cancelTextIOS={'Exit'}
                                confirmTextIOS={'OK'}
                                isVisible={this.state.isVisible2}
                                onConfirm={this.handleEndTimePicker}
                                onCancel={this.hideEndTimePicker}
                                mode={'time'}
                                format={'HH:mm'}
                                is24Hour={true}
                                å
                                datePickerModeAndroid={'spinner'}
                                textColor={colors.black}
                                name="endTime"
                                value={this.state.endTime}
                                onChange={this.timechange2}
                            />
                            <Text
                                style={{
                                    color: 'black',
                                    fontSize: 20,
                                    margin: 10,
                                }}
                            >
                                {this.state.endTime !== null &&
                                    moment(this.state.endTime).format('HH:mm')}
                            </Text>
                        </View>

                        <Text>Startdate:</Text>

                        <SectionedMultiSelect
                            items={items}
                            uniqueKey="name"
                            selectText="Choose some things..."
                            showDropDowns={true}
                            readOnlyHeadings={true}
                            onSelectedItemsChange={this.onSelectedItemsChange}
                            selectedItems={this.state.day}
                            name="day"
                            value={this.state.day}
                        />

                        <Text>Classroom:</Text>
                        <TextInput
                            style={styles.input}
                            placeholder="Course Location?"
                            textContentType={'location'}
                            keyboardType="default"
                            maxLength={30}
                            onChangeText={(text) =>
                                this.setState({ location: text })
                            }
                            name="location"
                            value={this.state.location}
                        />

                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                marginTop: 12,
                            }}
                        >
                            {this.renderColors()}
                        </View>

                        <TouchableOpacity
                            style={[
                                styles.create,
                                { backgroundColor: this.state.color },
                            ]}
                            type="submit"
                            onPress={this.createTodo}
                        >
                            <Text
                                style={{
                                    color: colors.white,
                                    fontWeight: '600',
                                }}
                            >
                                Create!
                            </Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 28,
        fontWeight: '800',
        color: colors.black,
        alignSelf: 'center',
        marginBottom: 16,
    },
    input: {
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: colors.blue,
        borderRadius: 6,
        height: 50,
        marginTop: 8,
        paddingHorizontal: 16,
        fontSize: 18,
    },
    create: {
        marginTop: 24,
        height: 50,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    colorSelect: {
        width: 30,
        height: 30,
        borderRadius: 4,
    },
    button: {
        width: 250,
        height: 50,
        backgroundColor: '#330066',
        borderRadius: 30,
        justifyContent: 'center',
        marginBottom: 20,
    },
    text: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
    },
});
